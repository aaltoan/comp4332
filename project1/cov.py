import small_data as sd
import numpy as np

if __name__=="__main__":
    data = sd.SmallData(dataset="data/orange_small_train")
    data.fill_single_val_col()
    data.fill_missing_values()
    data.scale()
    data.one_hot_encode()

    # some statistics about categorical values
    #
    print([(i, col.nunique(), col.count()) for i, col in data.cat.iteritems()])

    # Correlation is probably more meaningful:

    # Pairwise Pearson correlation matrix
    correlations = data.num.corr(min_periods=5)
    correlations -= np.identity(correlations.shape[0])
    print(correlations)
    # Result: There are some perfectly correlating attributes (R≈1.0)

    # Attribute-label correlations
    appetency_correlations = [col.corr(data.appetency) for i, col in data.num.iteritems()]
    appetency_correlations += [col.corr(data.appetency) for i, col in data.cat_dummies.iteritems()]
    churn_correlations = [col.corr(data.appetency) for i, col in data.num.iteritems()]
    churn_correlations += [col.corr(data.appetency) for i, col in data.cat_dummies.iteritems()]
    upselling_correlations = [col.corr(data.appetency) for i, col in data.num.iteritems()]
    upselling_correlations += [col.corr(data.appetency) for i, col in data.cat_dummies.iteritems()]
    # Result: Attribute label-correlations don't seem significant (maximum R=0.13).
