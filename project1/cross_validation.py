from __future__ import print_function
from sklearn.cross_validation import StratifiedKFold
from small_data import SmallData
from sklearn.metrics import roc_auc_score
import numpy as np
from sklearn.decomposition import PCA
from pandas import DataFrame
from scipy.stats import itemfreq


class CrossValidation:
    def __init__(self, n_folds=6, PCA_enabled=False, explained_threshold=0.9, print_auc=False, clf_average=True,
                 nrows=None):
        self.raw_dataset = SmallData("data/orange_small_train", nrows=nrows)
        self.raw_dataset.one_hot_encode(replace=True, to_numerical=True)
        self.raw_dataset.fill_single_val_col()
        self.raw_dataset.fill_missing_values()
        self.raw_dataset.scale()
        self.labels = ['appetency', 'churn', 'upselling']
        self.cvs = [(label, StratifiedKFold(self.raw_dataset.__dict__[label], n_folds=n_folds)) for label in self.labels]
        self.auc_scores = {}  # holding AUC scores
        if PCA_enabled:
            pca = PCA(n_components=len(self.raw_dataset.keys_num))
            pca.fit(self.raw_dataset.df[self.raw_dataset.keys_num])
            transformed_data = pca.transform(self.raw_dataset.df[self.raw_dataset.keys_num])
            last_eigen = np.where(np.cumsum(pca.explained_variance_ratio_) > explained_threshold)[0][0]
            self.pca_dataset = SmallData(frame=DataFrame(transformed_data[:, :last_eigen]),
                                         appetency=self.raw_dataset.appetency,
                                         churn=self.raw_dataset.churn,
                                         upselling=self.raw_dataset.upselling)
            self.pca_dataset.keys_num = self.pca_dataset.df.columns
        self.print_auc = print_auc
        self.clf_average = clf_average

    # Redesign signature: list of tuples: [(PCABayesClassifier, True), (RandomForestClassifier, False)]
    def validate(self, classifiers):
        proba_dict = {}
        for label, cv in self.cvs:
            for i, (train_partition, test_partition) in enumerate(cv):
                for clf, PCA_enabled in classifiers:
                    if PCA_enabled:
                        data = self.pca_dataset
                    else:
                        data = self.raw_dataset
                    train_data = SmallData(frame=data.df.iloc[train_partition, :].copy(),
                                           appetency=data.appetency.iloc[train_partition],
                                           upselling=data.upselling.iloc[train_partition],
                                           churn=data.churn.iloc[train_partition])
                    y = train_data.__dict__[label]
                    clf.fit(train_data.num, np.ravel(y))

                    test_data = SmallData(frame=data.df.iloc[test_partition, :].copy(),
                                          appetency=data.appetency.iloc[test_partition],
                                          upselling=data.upselling.iloc[test_partition],
                                          churn=data.churn.iloc[test_partition])

                    proba_dict[clf.__class__.__name__] = clf.predict_proba(test_data.num)

                combined_proba = self.combined_proba(proba_dict, test_partition, y, classifiers)

                score = roc_auc_score(np.array(test_data.__dict__[label]) == 1, [p[1] for p in combined_proba])
                if label in self.auc_scores:
                    self.auc_scores[label].append(score)
                else:
                    self.auc_scores[label] = []
                    self.auc_scores[label].append(score)
                if self.print_auc:
                    print("AUC score", label, score)
                else:
                    print(".", end="")
        print("")

    def auc_averages(self):
        return {label: sum(scores) / len(scores) for label, scores in self.auc_scores.iteritems()}

    def combined_proba(self, proba_dict, test, y, classifiers):
        if self.clf_average:
            combined_proba = np.zeros([len(test), 2])
            for _, clf_proba in proba_dict.iteritems():
                combined_proba += clf_proba
            combined_proba /= len(classifiers)
        else:
            combined_proba = np.ones([len(test), 2])
            for _, clf_proba in proba_dict.iteritems():
                combined_proba *= clf_proba
            priors = np.tile([count / float(len(y)) for label, count in itemfreq(np.array(y))], (len(test), 1))
            combined_proba /= priors
        return combined_proba

if __name__ == "__main__":
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.naive_bayes import GaussianNB

    repeats = 5
    n_estimators = 35
    max_depth = 8

    cv = CrossValidation(n_folds=6, PCA_enabled=False)
    for i in range(0, repeats):
        cv.validate([(RandomForestClassifier(n_jobs=-1, n_estimators=n_estimators, max_depth=max_depth), False)])
    print("RandomForestClassifier", max_depth, n_estimators, cv.auc_averages())
    del cv

    cv = CrossValidation(n_folds=6, PCA_enabled=True)
    for i in range(0, repeats):
        cv.validate([(RandomForestClassifier(n_jobs=-1, n_estimators=n_estimators, max_depth=max_depth), True)])
    print("PCARandomForestClassifier", max_depth, n_estimators, cv.auc_averages())
    del cv