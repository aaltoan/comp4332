TODO for Project 1
==================

- ~~create a git repository~~
- ~~read the data with python (both small and large)~~
- ~~correct the alignment problems in the data~~
- ~~plot distributions for the numerical data~~
    - separately for each real class
- dimensionality reduction
    - PCA with the small data set
        1. numerical attributes
            - ~~normalize the numerical attributes (decimal scaling or z-index)~~
        2. separate categorical attributes to their own columns by type and try with them too
            - ~~is there a better way?~~
            - no silver bullet, but there are several things we can try
            (references: http://www.unc.edu/~skolenik/talks/Gustavo-Stas-PCA-generic.pdf)
- classification
    - SVM for the eigenvectors of PCA (hard?)
    - kNN (Sam)
- naive Bayes
    - ~~try out naive Bayes with at least some data~~
    - ~~calculate correlation of attributes with the class (Antti)~~
    - ~~calculate correlation of attributes with each other (Antti)~~
    - research how robust NB is with obviously wrong prior (first four eigenvectors)
- ~decision tree (Antti)~
    - the most used method in KDDCUP '09
- ~~confusion matrix~~
- ~~create an outline for the assignment report~~
- preprosessing
    - ~~replace missing values (Antti)~~
        - ~~mean for numerical~~
    - ~~create dummy variables (binarize categorical attributes) (Antti)~~
- ~cross-validation~
- cross validate PCA Naïve Bayesian
- try AdaBoost with PCA NB and RandomForestClassifier?

Tasks
-----
- convert the categorical attributes to a more readable format

Identified challenges
---------------------
- outliers
    - drop them?
- missing values
    - missing entropy method works only with positive values
    - four attributes with also negative values
