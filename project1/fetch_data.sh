#!/bin/sh

cd data
wget -c http://www.sigkdd.org/site/2009/files/orange_small_train.data.zip
wget -c http://www.sigkdd.org/site/2009/files/orange_small_test.data.zip

unzip orange_small_train.data.zip
unzip orange_small_test.data.zip

wget http://www.sigkdd.org/site/2009/files/orange_small_train_appetency.labels
wget http://www.sigkdd.org/site/2009/files/orange_small_train_churn.labels
wget http://www.sigkdd.org/site/2009/files/orange_small_train_upselling.labels

