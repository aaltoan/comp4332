from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.cross_validation import StratifiedKFold
import small_data as sd
from sklearn.naive_bayes import GaussianNB
from pandas import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    dataset = "data/orange_small_train"
    raw_data_frame = sd.SmallData(dataset)

    cv = StratifiedKFold(raw_data_frame.appetency, n_folds=6)

    raw_data_frame.one_hot_encode(replace=True, to_numerical=True)
    raw_data_frame.fill_single_val_col()
    raw_data_frame.fill_missing_values()
    raw_data_frame.scale()

    for i, (train, test) in enumerate(cv):
        train_data = sd.SmallData(frame=raw_data_frame.df.iloc[train, :].copy(),
                                appetency=raw_data_frame.appetency.iloc[train],
                                upselling=raw_data_frame.upselling.iloc[train],
                                churn=raw_data_frame.churn.iloc[train])

        appetency_classifier = GaussianNB()
        appetency_classifier.fit(train_data.num, np.ravel(train_data.appetency))

        test_data = sd.SmallData(frame=raw_data_frame.df.iloc[test, :].copy(),
                                 appetency=raw_data_frame.appetency.iloc[test],
                                 upselling=raw_data_frame.upselling.iloc[test],
                                 churn=raw_data_frame.churn.iloc[test])

        prob_appetency = appetency_classifier.predict_proba(test_data.num)
        print("appetency AUC score", roc_auc_score(np.array(test_data.appetency) == 1, prob_appetency[:, 1]))
