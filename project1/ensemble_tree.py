from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from cross_validation import CrossValidation
from pca_bayes_tree_classifier import PCABayesTreeClassifier

if __name__ == "__main__":
    cv = CrossValidation(n_folds=6, PCA_enabled=True)
    #cv.validate(PCABayesTreeClassifier())
    for i in range(1, 3):
        cv.validate([(RandomForestClassifier(n_jobs=-1), False), (GaussianNB(), True)])
    #cv2 = CrossValidation(n_folds=6, PCA_enabled=True)
    #cv2.validate(GaussianNB())
    # cv.validate(RandomForestClassifier(n_jobs=-1))
    print(cv.auc_averages())