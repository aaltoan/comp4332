import small_data as sd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn import svm
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
import time
from cross_validation import CrossValidation

if __name__ == "__main__":
    cv = CrossValidation(n_folds=6, PCA_enabled=False)
    cv.validate([(svm.SVC(kernel='linear', probability=True), False)])
    print(cv.auc_averages())
    del cv

    cv = CrossValidation(n_folds=6, PCA_enabled=True)
    cv.validate([(svm.SVC(kernel='linear', probability=True), True)])
    print(cv.auc_averages())
    # """
    # demonstrate how to compute an ROC curve and AOC score.
    # """
    # data = sd.SmallData(dataset="data/orange_small_train")
    # data.fill_single_val_col()
    # data.fill_missing_values()
    # data.scale()
    # data.one_hot_encode(replace=True)
    #
    # pca = PCA(n_components=len(data.keys_num))
    # pca.fit(data.df[data.keys_num])
    # plt.figure()
    # plt.plot(np.cumsum(pca.explained_variance_ratio_))
    # plt.ylim([0, 1])
    # plt.xlabel("# of eigenvectors", fontsize=16)
    # plt.ylabel("explained variance ratio", fontsize=16)
    #
    # transformed_train = pca.transform(data.df[data.keys_num])
    #
    # x_r = transformed_train[:, :100]
    # # x_r = data.df[data.keys_num]
    # start = time.time()
    # classifier = svm.SVC(kernel='linear', probability=True)
    # classifier.fit(x_r, np.ravel(data.upselling))
    # # we need predicted probability instead of just classification results
    # predicted_prob = classifier.predict_proba(x_r)
    # y = np.array(data.upselling)
    # # predicted_prob is a n_samples x n_classes array
    # # n_classes is ordered from small to largesc
    # # so column 1 corresponds to positive label
    # scores = predicted_prob[:, 1]
    # fpr, tpr, thresholds = roc_curve(y, scores, pos_label=1)
    #
    # plt.figure()
    # plt.plot(fpr, tpr)
    # plt.xlabel("False Positive Rate", fontsize=16)
    # plt.ylabel("True Positive Rate", fontsize=16)
    # plt.title("upselling", fontsize=16)
    #
    # print("upselling AUC Score: ", roc_auc_score(y == 1, predicted_prob[:, 1]))
    #
    # classifier.fit(x_r, np.ravel(data.churn))
    # # we need predicted probability instead of just classification results
    # predicted_prob = classifier.predict_proba(x_r)
    # y = np.array(data.churn)
    # # predicted_prob is a n_samples x n_classes array
    # # n_classes is ordered from small to largesc
    # # so column 1 corresponds to positive label
    # scores = predicted_prob[:, 1]
    # fpr, tpr, thresholds = roc_curve(y, scores, pos_label=1)
    #
    # plt.figure()
    # plt.plot(fpr, tpr)
    # plt.xlabel("False Positive Rate", fontsize=16)
    # plt.ylabel("True Positive Rate", fontsize=16)
    # plt.title("churn", fontsize=16)
    #
    # print("churn AUC Score: ", roc_auc_score(y == 1, predicted_prob[:, 1]))
    #
    # classifier.fit(x_r, np.ravel(data.appetency))
    # # we need predicted probability instead of just classification results
    # predicted_prob = classifier.predict_proba(x_r)
    # y = np.array(data.appetency)
    # # predicted_prob is a n_samples x n_classes array
    # # n_classes is ordered from small to largesc
    # # so column 1 corresponds to positive label
    # scores = predicted_prob[:, 1]
    # fpr, tpr, thresholds = roc_curve(y, scores, pos_label=1)
    #
    # plt.figure()
    # plt.plot(fpr, tpr)
    # plt.xlabel("False Positive Rate", fontsize=16)
    # plt.ylabel("True Positive Rate", fontsize=16)
    # plt.title("appetency", fontsize=16)
    #
    # print("upselling AUC Score: ", roc_auc_score(y == 1, predicted_prob[:, 1]))
    #
    # plt.show()
    #
    # elapsed = (time.time() - start)
    # print("Elapsed: ", elapsed)