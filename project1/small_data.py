#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series
import numpy as np
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_extraction import DictVectorizer
import os


class SmallData(object):
    def __init__(self,
                 dataset=None,
                 nrows=None,
                 frame=None,
                 appetency=None, churn=None, upselling=None
                 ):
        if dataset:
            self.df, self.keys_num, self.keys_cat = self.read_data(dataset, nrows)
            self.appetency, self.churn, self.upselling = self.read_labels(dataset, nrows)
        else:
            self.df = frame
            self.appetency, self.churn, self.upselling = appetency, churn, upselling
            # the first 190 columns are numerical data,
            # the rest are categorical data
            self.keys_num = [key for key in self.df.columns if key < 191]
            self.keys_cat = [key for key in self.df.columns if key >= 191]

    def get_num(self):
        return self.df[self.keys_num]

    def get_cat(self):
        return self.df[self.keys_cat]

    num = property(get_num)
    cat = property(get_cat)

    def basic_preprocessing(self):
        """
        needed if the dataset is passed as a bare dataframe

        """
        # by default, the keys of the columns are strings
        # here we convert them to integers
        self.df.columns = [int(label[3:]) for label in self.df.columns]

        # eliminate columns that contain only NA (missing values)
        df = self.df.dropna(axis=1, how='all')

        # the first 190 columns are numerical data,
        # the rest are categorical data
        self.keys_num = [key for key in df.columns if key < 191]
        self.keys_cat = [key for key in df.columns if key >= 191]

    def read_data(self, dataset, nrows=None):
        """
        returning a pandas dataframe object, a list of column keys for numeric data
        and a list of column keys for categorical data

        :param dataset name
        :return: resulting data frame object
        """
        # read small training data
        df = pd.read_table(dataset + ".data", nrows=nrows)

        # by default, the keys of the columns are strings
        # here we convert them to integers
        df.columns = [int(label[3:]) for label in df.columns]

        # eliminate columns that contain only NA (missing values)
        df = df.dropna(axis=1, how='all')

        # the first 190 columns are numerical data,
        # the rest are categorical data
        keys_num = [key for key in df.columns if key < 191]
        keys_cat = [key for key in df.columns if key >= 191]

        return df, keys_num, keys_cat

    def read_labels(self, dataset, nrows=None):

        # by setting squeeze=True, single column pandas DataFrame will be converted to
        # a pandas Series
        appetency = pd.read_table(dataset + "_appetency.labels",
                                  header=None,
                                  names=['appetency'],
                                  squeeze=True,
                                  nrows=nrows)
        churn = pd.read_table(dataset + "_churn.labels",
                              header=None,
                              names=['churn'],
                              squeeze=True,
                              nrows=nrows)
        upselling = pd.read_table(dataset + "_upselling.labels",
                                  header=None,
                                  names=['upselling'],
                                  squeeze=True,
                                  nrows=nrows)
        return appetency, churn, upselling

    def plot_histogram(self):
        if not os.path.exists("./var_hist"):
            os.makedirs("./var_hist")

        for key in self.keys_num:
            plt.clf()
            self.df[key].hist(bins=50)
            plt.savefig("./var_hist/var"+str(key)+".pdf", format="pdf")

        # for categorical data, we can just plot the number of occurrence for each unique value
        for key in self.keys_cat:
            plt.clf()
            self.df[key].value_counts().plot(style='o')
            plt.savefig("./var_hist/var"+str(key)+".pdf", format="pdf")

    def plot_histogram_no_outlier(self):
        """
        plot histograms for columns after eliminating outliers
        outliers are defined to be data points beyond 3 sigma here

        """
        if not os.path.exists("./var_hist_no_outlier"):
            os.makedirs("./var_hist_no_outlier")

        for key in self.keys_num:
            plt.clf()
            self.df[key][self.df[key] < self.df[key].mean() + self.df[key].std()*3].hist(bins=50)
            plt.savefig("./var_hist_no_outlier/var"+str(key)+".pdf", format="pdf")

        # for categorical data, we can just plot the number of occurrence for each unique value
        for key in self.keys_cat:
            plt.clf()
            self.df[key].value_counts().plot(style='o')
            plt.savefig("./var_hist_no_outlier/var"+str(key)+".pdf", format="pdf")

    def del_single_val_col(self):
        """
        delete columns that have only one value in addition to missing values

        """
        deleted_num_columns = 0
        deleted_cat_columns = 0
        for key in self.keys_num:
            if len(self.df[key].value_counts()) == 1:
                self.keys_num.remove(key)
                del self.df[key]
                deleted_num_columns += 1

        for key in self.keys_cat:
            if len(self.df[key].value_counts()) == 1:
                self.keys_cat.remove(key)
                del self.df[key]
                deleted_cat_columns += 1
        return deleted_num_columns, deleted_cat_columns

    def fill_single_val_col(self, fill_vals=None):
        """
        replace nans with zeros for those columns that only have one other value
        will store the columns that are filled and the fill value for test data

        """
        if fill_vals:
            for key, fill_value in fill_vals.iteritems():
                self.df[key].fillna(fill_value)
        else:
            self.fill_vals = {}
            filled_columns = 0
            for key, col in self.num.iteritems():
                if col.nunique() == 1:
                    fill_value = 0 if col[col.notnull()].iget(0) else 1
                    col.fillna(fill_value)
                    filled_columns += 1
                    self.fill_vals[key] = fill_value
            return filled_columns

    def fill_missing_values(self, imp_trans=None):
        """
        replace missing numeric values with their means
        will store the transformer for test data

        """
        if imp_trans:
            self.df[self.keys_num] = imp_trans.transform(self.df[self.keys_num])
        else:
            self.imp_transform = Imputer(strategy="mean", axis=0)
            self.imp_transform.fit(self.df[self.keys_num])
            self.df[self.keys_num] = self.imp_transform.transform(self.df[self.keys_num])

            self.df[self.keys_cat] = self.df[self.keys_cat].fillna("9ehgpw9853wrgw")

    def scale(self, scaler=None):
        if scaler:
            self.df[self.keys_num] = scaler.transform(self.df[self.keys_num])
        else:
            self.min_max_scaler = MinMaxScaler(feature_range=(-1, 1))
            self.df[self.keys_num] = self.min_max_scaler.fit_transform(self.df[self.keys_num])

    def one_hot_encode(self, threshold=1000, replace=False, to_numerical=True):
        """
        replace categorical values with binary dummy values

        :param threshold maximum number of unique category names per attribute
        """

        # TODO adapt the function so that it can be applied to test data

        # Originally based on this gist, although it no more looks much like it.
        # https://gist.github.com/kljensen/5452382

        # some columns have too many different values to be usable
        keys_cat_to_process = [i for i, col in self.cat.iteritems() if col.nunique() < threshold]
        categorical_attributes = self.cat[keys_cat_to_process]
        v = DictVectorizer()
        # to_dict with outtype="records" takes 15-30 s
        numpy_sparse_matrix = v.fit_transform(categorical_attributes.to_dict(outtype="records"))
        # Ideally we'd transform the sparse matrix to a DataFrame and use it.
        # However, with too large threshold the following uses all memory and dies:
        numpy_array = numpy_sparse_matrix.toarray()
        self.cat_dummies = pd.DataFrame(numpy_array)
        self.cat_dummies.columns = v.get_feature_names()
        # at this stage, self.cat_dummies contains some of the original categorical columns,
        # which is really unexpected. the following two lines are to remove those columns
        keys_to_drop = [key for key in keys_cat_to_process if key in self.cat_dummies.columns]
        self.cat_dummies = self.cat_dummies.drop(keys_to_drop, axis=1)

        if replace is True:
            # we discard all categorical columns including those who do not make it into cat_dummies
            self.df = self.df.drop(self.keys_cat, axis=1)
            self.df = self.df.join(self.cat_dummies)
            if to_numerical:
                # The columns can now be handeld as numerical
                self.keys_num += self.cat_dummies.columns
                self.keys_cat = []
            else:
                # For some classifiers we still want to keep the separation.
                # E.g. naïve Bayes might benefit from knowing the distributions.
                self.keys_num = [key for key in self.df.columns if key < 191]
                self.keys_cat = [key for key in self.df.columns if key >= 191]


    def complete_columns(self):
        return SmallData(frame=self.df.dropna(axis=1), appetency=self.appetency,
                         churn=self.churn, upselling=self.upselling)


# for tests and usage
if __name__ == "__main__":
    data = SmallData(dataset="data/orange_small_train")
    data.fill_single_val_col()
    data.fill_missing_values()
    data.scale()
    data.one_hot_encode(replace=True)