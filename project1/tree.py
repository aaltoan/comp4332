import small_data as sd
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier

if __name__ == "__main__":
    data = sd.SmallData(dataset="data/orange_small_train")
    data.fill_single_val_col()
    data.fill_missing_values()
    data.scale()

    appentency_tree = DecisionTreeClassifier()
    appentency_tree.fit(data.num, np.ravel(data.appetency))
    predicted_appetency = appentency_tree.predict(data.num)
    prob_appetency = appentency_tree.predict_proba(data.num)
    print("appetency AUC score", roc_auc_score(np.array(data.appetency) == 1, prob_appetency[:, 1]))
    print(confusion_matrix(np.ravel(data.appetency), predicted_appetency))

    churn_tree = DecisionTreeClassifier()
    churn_tree.fit(data.num, np.ravel(data.churn))
    predicted_churn = churn_tree.predict(data.num)
    prob_churn = churn_tree.predict_proba(data.num)
    print("churn AUC score", roc_auc_score(np.array(data.churn) == 1, prob_churn[:, 1]))
    print(confusion_matrix(np.ravel(data.churn), predicted_churn))

    upselling_tree = DecisionTreeClassifier()
    upselling_tree.fit(data.num, np.ravel(data.upselling))
    predicted_upselling = upselling_tree.predict(data.num)
    prob_upselling = upselling_tree.predict_proba(data.num)
    print("upselling AUC score", roc_auc_score(np.array(data.upselling) == 1, prob_upselling[:, 1]))
    print(confusion_matrix(np.ravel(data.upselling), predicted_upselling))