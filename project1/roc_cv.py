# for COMP4332 assignment 2
# inspired by http://scikit-learn.org/stable/auto_examples/plot_roc_crossval.html

import matplotlib.pyplot as plt
import numpy as np
from cross_validation import CrossValidation
from small_data import SmallData
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, roc_curve, auc
from sklearn.cross_validation import StratifiedKFold

if __name__ == "__main__":
    raw_dataset = SmallData("data/orange_small_train")
    raw_dataset.one_hot_encode(replace=True, to_numerical=True)
    raw_dataset.fill_single_val_col()
    raw_dataset.fill_missing_values()
    raw_dataset.scale()

    cv = StratifiedKFold(np.ravel(raw_dataset.upselling), n_folds=6)

    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 1000)

    upselling_clf = RandomForestClassifier(n_jobs=-1, n_estimators=35, max_depth=8)

    for i, (train_partition, test_partition) in enumerate(cv):
        data = raw_dataset
        train_data = SmallData(frame=data.df.iloc[train_partition, :].copy(),
                               appetency=data.appetency.iloc[train_partition],
                               upselling=data.upselling.iloc[train_partition],
                               churn=data.churn.iloc[train_partition])
        y = np.ravel(raw_dataset.__dict__["upselling"])

        test_data = SmallData(frame=data.df.iloc[test_partition, :].copy(),
                              appetency=data.appetency.iloc[test_partition],
                              upselling=data.upselling.iloc[test_partition],
                              churn=data.churn.iloc[test_partition])

        upselling_clf.fit(train_data.num, y[train_partition])
        probas_ = upselling_clf.predict_proba(test_data.num)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y[test_partition], probas_[:, 1])
        mean_tpr += np.interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (i, roc_auc))

    mean_tpr /= len(cv)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    plt.plot(mean_fpr, mean_tpr, 'k--',
             label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

    upselling_clf.fit(raw_dataset.df, np.ravel(raw_dataset.upselling))
    upselling_proba = upselling_clf.predict_proba(raw_dataset.df)[:, 1]
    roc_auc = roc_auc_score(np.array(raw_dataset.upselling) == 1, upselling_proba)

    fpr, tpr, thresholds = roc_curve(np.ravel(raw_dataset.upselling), upselling_proba)
    plt.plot(fpr, tpr, '--', color=(0.6, 0.6, 0.6), label='ROC (whole training set) (area = %0.2f)' % (roc_auc))

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate', fontsize=16)
    plt.ylabel('True Positive Rate', fontsize=16)
    plt.legend(loc="lower right")
    plt.savefig('assig2_roc.pdf', format='pdf', bbox_inches='tight')

    plt.show()


