import small_data as sd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from cross_validation import CrossValidation

if __name__ == "__main__":
    cv = CrossValidation(n_folds=6, PCA_enabled=False)
    cv.validate([(KNeighborsClassifier(n_neighbors=10), False)])
    print(cv.auc_averages())
    del cv

    cv = CrossValidation(n_folds=6, PCA_enabled=True)
    cv.validate([(KNeighborsClassifier(n_neighbors=10), True)])
    print(cv.auc_averages())