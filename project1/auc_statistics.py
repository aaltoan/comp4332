# for COMP4332 assignment 2

from small_data import SmallData
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score
from pandas import DataFrame
import numpy as np
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import roc_auc_score, roc_curve, auc

if __name__ == "__main__":
    raw_dataset = SmallData("data/orange_small_train")
    raw_dataset.one_hot_encode(replace=True, to_numerical=True)
    raw_dataset.fill_single_val_col()
    raw_dataset.fill_missing_values()
    raw_dataset.scale()

    explained_threshold = 0.9

    pca = PCA(n_components=len(raw_dataset.keys_num))
    pca.fit(raw_dataset.df[raw_dataset.keys_num])
    transformed_data = pca.transform(raw_dataset.df[raw_dataset.keys_num])
    last_eigen = np.where(np.cumsum(pca.explained_variance_ratio_) > explained_threshold)[0][0]
    pca_dataset = SmallData(frame=DataFrame(transformed_data[:, :last_eigen]),
                            appetency=raw_dataset.appetency,
                            churn=raw_dataset.churn,
                            upselling=raw_dataset.upselling)
    pca_dataset.keys_num = pca_dataset.df.columns

    appetency_clf = RandomForestClassifier(n_jobs=-1, n_estimators=35, max_depth=8)
    # appetency_clf.fit(pca_dataset.df, np.ravel(pca_dataset.appetency))
    # appetency_proba = appetency_clf.predict_proba(pca_dataset.df)[:, 1]
    # print("appetency AUC", roc_auc_score(np.array(pca_dataset.appetency) == 1, appetency_proba))

    churn_clf = RandomForestClassifier(n_jobs=-1, n_estimators=35, max_depth=8)
    # churn_clf.fit(raw_dataset.df, np.ravel(raw_dataset.churn))
    # churn_proba = churn_clf.predict_proba(raw_dataset.df)[:, 1]
    # print("churn AUC", roc_auc_score(np.array(pca_dataset.churn) == 1, churn_proba))

    upselling_clf = RandomForestClassifier(n_jobs=-1, n_estimators=35, max_depth=8)
    # upselling_clf.fit(raw_dataset.df, np.ravel(raw_dataset.upselling))
    # upselling_proba = upselling_clf.predict_proba(raw_dataset.df)[:, 1]
    # print("upselling AUC", roc_auc_score(np.array(pca_dataset.upselling) == 1, upselling_proba))

    repeats = 1

    # (classifier, PCA_enabled?, label)
    classifiers = [
        (appetency_clf, True, "appetency"),
        (churn_clf, False, "churn"),
        (upselling_clf, False, "upselling")
    ]

    auc_dict = {}

    for re in range(repeats):
        cv = StratifiedKFold(np.ravel(raw_dataset.upselling), n_folds=6)

        for i, (train_partition, test_partition) in enumerate(cv):
            for (clf, PCA_enabled, label) in classifiers:
                if PCA_enabled:
                    data = pca_dataset
                else:
                    data = raw_dataset
                train_data = SmallData(frame=data.df.iloc[train_partition, :].copy(),
                                       appetency=data.appetency.iloc[train_partition],
                                       upselling=data.upselling.iloc[train_partition],
                                       churn=data.churn.iloc[train_partition])
                y = np.ravel(raw_dataset.__dict__[label])

                test_data = SmallData(frame=data.df.iloc[test_partition, :].copy(),
                                      appetency=data.appetency.iloc[test_partition],
                                      upselling=data.upselling.iloc[test_partition],
                                      churn=data.churn.iloc[test_partition])

                clf.fit(train_data.num, y[train_partition])
                probas_ = clf.predict_proba(test_data.num)

                # Compute ROC curve and area the curve
                fpr, tpr, thresholds = roc_curve(y[test_partition], probas_[:, 1])
                if label not in auc_dict:
                    auc_dict[label] = []
                auc_dict[label].append(auc(fpr, tpr))

    for (label, scores) in auc_dict.iteritems():
        print(label, "mean: ", np.mean(np.array(scores)), "std: ", np.std(np.array(scores)))