import numpy as np
from scipy.stats import itemfreq
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier

class PCABayesTreeClassifier:
    """
    This is a behavioral subclass of GaussianNB (and most similar classifier in sklearn).
    """
    def __init__(self):
        self.gaussianNB = GaussianNB()
        self.tree = DecisionTreeClassifier()

    def fit(self, X, y):
        self.gaussianNB.fit(X, y)
        self.tree.fit(X, y)
        self.priors = [count / float(len(y)) for label, count in itemfreq(y)]

    def predict_proba(self, X):
        gaussian_proba = self.gaussianNB.predict_proba(X)
        tree_proba = self.tree.predict_proba(X)
        combined_proba = []
        for gaussian, tree in zip(gaussian_proba, tree_proba):
            # TODO try also other ways of combining probabilities
            # 1. weighted vote by averaging the probabilities
            combined_proba.append([g * t / prior for g, t, prior in zip(gaussian, tree, self.priors)])
        return combined_proba
