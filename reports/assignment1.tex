% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

\title{COMP4332 Assignment 1}
\author{AALTO Antti and WU Degang}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle


\section{Introduction}
To predict the behaviors of telecommunication customer we have analyzed the dataset, tried to preprocess it in multiple ways and tried to create a classifier based on PCA and naïve Bayes techniques.
To enhance the results, we continue cleaning up the data. We will extend by creating an ensemble of other classifiers, including at least SVM, decision trees and kNN.

We have found out that there are many missing values, differing ranges and skewed distributions. Some data is redundant. The real classes of the training data are highly imbalanced and there are way more negative than positive class labels.

\section{Remarks on the data}

Thus far we have only explored the small dataset. In the project we will also try to apply the discussed techniques to the large data set. However, some of our techniques are not applicable to the large data set. Especially, PCA has time complexity of $\mathcal{O}(n^2)$, where $n$ is the number of features.

The dataset is sparse, i.e., there are lots of missing values. $70\%$ of the data entries are missing. 18 columns in the small dataset contain only missing values. These columns are entirely useless and are eliminated. Among the remaining columns, the column that contains the least non-null values has only 171 non-null value. Without any useful information about the nature of the columns, we decided to fill out the missing numerical values with the mean of the corresponding column. We also considered using an entropy-based imputation method, but with Bayesian methods it might not be easily justified. For categorical values we will create binary dummy variables, and with them the missing values will be handled as their own separate category.

Five of the columns only have one distinct values in addition to the missing values. These columns will be eliminated for now. An alternative scheme would be to fill the missing values with zeroes.

The ranges of numerical attributes vary wildly. Four columns contain negative values. For some columns the maximum value is in hundreds of millions, for others it barely differs from zero. The maximum difference between maximum and minimum is 442,233,600, while some attributes have only one distinct value. This means that we have to normalize the attributes. Otherwise the large values would have disproportionate effect on the prediction.

Most numerical columns are skewed to the right, or positively skewed. For this reason, any methods that assume a Gaussian distribution won't work very well in principle on the raw data. The distributions of the categorical attributes were inspected graphically. We expect them to follow some multinomial distribution.

The dataset is highly imbalanced, i.e., there are far more negative observations than positive observations. For churn and upselling, only $7\%$ of the observations are positive. For appetency, only $2\%$ of the observations are positive. This fact makes prediction accuracy a really bad choice for the quality evaluation for the prediction results.

Maxima of attribute-label Pearson correlation coefficients are 0.119 for appetency, 0.118 for churn and 0.213 for upselling. No single attribute stands out as a clear indicator of any class. Thus we have to combine many or all of the attributes to get higher confidence levels for our classifier.

Between the attributes, pairwise correlations vary from -0.237 to 1.0. Some of the highly correlated attributes contain no information. For example, attribute Var66 seems to be exactly twice the value of attribute Var9 except for three of the 694 records for which both attributes are given.

\section{Preprocessing procedures}

As usual, preprocessing seems to be the most tedious phase of creating a classifier. We try to address the problems noted in the previous section by the following procedures:

\begin{enumerate}
	\item{Delete columns that contain only missing values.}
	\item{Delete redundant columns.}
	\item{Fill columns that contain only one distinct non-null value with some other value.}
	\item{MinMax normalize the numerical columns to range from -1 to 1.}
	\item{Replace the missing values with mean in numerical columns.}
	\item{Binarize the categorical columns.}
\end{enumerate}

Computationally the biggest procedure is binarizing the categorical attributes with one hot encoding.\footnote{http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.OneHotEncoder.html} This allows us to apply many classifiers, which cannot process nominal string values, to the dataset. Especially, PCA requires all the attributes to have numerical values.

\section{Feature extraction}
We have applied PCA for extracting the most significant eigenvectors from the preprocessed data. Their eigenvalues indicate how much of the variance in the data the eigenvectors explain. For instance, the first 10 vectors explain 86 \% and the first 30 explain 96\%. In the next phase we select the most descriptive eigenvectors and apply several classifiers to them. PCA method works well for the numerical attributes of the small data set and runs in less than 5 s on a modern computer.

Binarizing the categorical attributes generates a huge number of boolean dummy attributes. This makes using PCA more expensive. We will try applying PCA to the categorical boolean attributes, but we expect that we need to find some other method for feature extraction from the categorical values.

\section{Classifiers}
Thus far, applying the naïve Bayes classifier to the raw data has resulted in poor results. Some of the poor performance might be explained by unclean data or high mutual correlation of the attributes. We find it also very interesting to see, if it is possible to get any good results by applying naïve Bayes to eigenvectors resulted from PCA at all.

The most important parameter of the naïve Bayes classifier seems to be the prior distribution. For most of the raw numerical data a normal distribution would seem a sensible default. However, there are some attributes which clearly come from some other distribution. For categorical values a multinomial distribution is probably better.
\footnote{http://scikit-learn.org/stable/modules/generated/sklearn.naive\_bayes.MultinomialNB.html}

During the next phase of the project, we will evaluate the predictive power of the following classifiers:
\begin{enumerate}
	\item{naïve Bayes}
	\item{decision trees}
	\item{kNN}
	\item{SVM}
\end{enumerate}

Our final goal is to create a robust ensemble classifier from some combination of these classifiers.

\section{Software}
We use Python's scikit-learn\footnote{http://scikit-learn.org} (including numpy, scipy and matplotlib) and pandas\footnote{http://pandas.pydata.org} (data mangling).

Our code is available at a Git repository at: https://bitbucket.org/aaltoan/comp4332

\end{document}
