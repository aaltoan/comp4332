% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% for highlighting
\usepackage{color}
\usepackage{soul}

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

\title{COMP4332 Project 1 Complete Report}
\author{AALTO Antti and WU Degang}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle


\section{Introduction}
To predict the behaviors of telecommunication customer we have analyzed the dataset, preprocessed it in multiple ways and created classifiers, including naïve Bayes classifiers, Support Vector Machine, K-Nearest-Neighbors and decision tree. We have also tried ensemble methods like random forests and simple weighted votes method. We have found that random forests ensemble method score the highest in performance with suitable parameters.

\section{Preprocessing steps}

\subsection{Remarks on the data}

We used the small dataset of 230 attributes and 50,000 samples. Some of our techniques were computationally so expensive that we decided that the large data set with 14,740 variables would be out of the scope. Especially, PCA has time complexity of $\mathcal{O}(n^2)$, where $n$ is the number of features. The following remarks were made for assignment 1 and are repeated here with updates.

The dataset is sparse, i.e., there are lots of missing values. $70\%$ of the data entries are missing. 18 columns in the small dataset contain only missing values. These columns are entirely useless and are eliminated. Among the remaining columns, the column that contains the least non-null values has only 171 non-null value. Without any useful information about the nature of the columns, we decided to fill out the missing numerical values with the mean of the corresponding column. We also considered using an entropy-based imputation method, but with Bayesian methods it might not be easily justified. For categorical values we will create binary dummy variables, and with them the missing values will be handled as their own separate category.

Five of the columns only have one distinct values in addition to the missing values. These columns will be eliminated for now. An alternative scheme would be to fill the missing values with zeroes.

The ranges of numerical attributes vary wildly. Four columns contain negative values. For some columns the maximum value is in hundreds of millions, for others it barely differs from zero. The maximum difference between maximum and minimum is 442,233,600, while some attributes have only one distinct value. This means that we have to normalize the attributes. Otherwise the large values would have disproportionate effect on the prediction.

Most numerical columns are skewed to the right, or positively skewed. For this reason, any methods that assume a Gaussian distribution won't work very well in principle on the raw data. The distributions of the categorical attributes were inspected graphically. We expect them to follow some multinomial distribution.

The dataset is highly imbalanced, i.e., there are far more negative observations than positive observations. For churn and upselling, only $7\%$ of the observations are positive. For appetency, only $2\%$ of the observations are positive. This fact makes prediction accuracy a really bad choice for the quality evaluation for the prediction results. AUC score will be the primary performance indicator. 

Maxima of attribute-label Pearson correlation coefficients are 0.119 for appetency, 0.118 for churn and 0.213 for upselling. No single attribute stands out as a clear indicator of any class. Thus we have to combine many or all of the attributes to get higher confidence levels for our classifier.

Between the attributes, pairwise correlations vary from -0.237 to 1.0. Some of the highly correlated attributes contain no information. For example, attribute Var66 seems to be exactly twice the value of attribute Var9 except for three of the 694 records for which both attributes are given. 

Regarding the categorical variables, some variables have more than 15,000 distinct levels, which pose serious challenges on the problem. 

\subsection{Preprocessing procedures}

As usual, preprocessing seems to be the most tedious phase of creating a classifier. The guiding principle of our design of the preprocessing procedures is to patch up the dataset so that classifiers can process with the dataset without problems while not to introduce any unjustified or under-justified assumptions into the dataset. Under this principle, we avoid doing aggressive data preprocessing such as aggregation of distinct levels for categorical variables. Also, we did not eliminate highly-correlated columns manually, due to lack of knowledge of methods that can properly eliminate redundant columns. We believe that mathematically-rigorous method like PCA will reliably remove the redundancy from the dataset.

For categorical variables, we use one-hot-encoding\footnote{http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.OneHotEncoder.html} to incorporate them into the numerical parts of the dataset. This allows us to apply many classifiers, which cannot process nominal string values, to the dataset. Especially, PCA requires all the attributes to have numerical values. One-hot-encoding has the merit of avoiding artificial correlation, compared to naive binarization. As mentioned in the last section, some categorical variables have more than 15,000 distinct levels, which poses serious challenge to one-hot-encoding scheme. In order not to introduce biased assumption into the dataset, we simply delete those categorical columns that have too much distinct levels. We set the threshold level to be 1,000, meaning that if a categorical column has more than 1,000 distinct levels, we will just remove it from the dataset. 

We try to address the problems noted in the previous section by the following procedures:

\begin{enumerate}
	\item{Delete columns that contain only missing values.}
	\item{Binarize the categorical columns using one-hot-encoding.}
	\item{Fill columns that contain only one distinct non-null value with some other value.}
	\item{Replace the missing values with mean in numerical columns.}
	\item{MinMax normalize the numerical columns to range from -1 to 1.}
\end{enumerate}

After the preprocessing, the dataset has 1,183 columns, of which 174 are numerical and the rest dummy variables for categorical attributes.

\subsection{Feature extraction}
We have applied PCA for extracting the most significant eigenvectors from the preprocessed data. Their eigenvalues indicate how much of the variance in the data the eigenvectors explain. 
In the preprocessed dataset, it takes the first 234 eigenvectors to explain $90\%$ of the variance. Graphical inspection show that the first 10 projections do not separate the positive classes from the negative classes. Projecting all data points onto the subspace spanned by the first 3 eigenvectors reveals no clear separation plane between the positive class from the negative class. While it is still possible that a separation plane can be found in high-dimensional space, at this point we do not expect SVM will work well on the dataset. However, as we will shown below, PCA does help the performance of naïve Bayes classifier.

\section{Classifiers}

\subsection{Naïve Bayes classifier}

The naïve Bayes classifier we use assumes a Gaussian distribution. Directly apply the naïve Bayes classifier on the preprocessed dataset without PCA transform yields AUC scores of 0.528 for label upselling, 0.520 for label churn and 0.495 for label appetency, which is quite bad. However, after PCA transform (we take the largest 234 eigenvalues), the AUC scores become 0.642 for label upselling, 0.608 for label churn and 0.635 for label appetency. All AUC scores mentioned above are calculated from 6-fold stratified cross-validation. PCA transform does help the naïve Bayes classifier.

\subsection{Support Vector Machine Classifier}

The SVM implementation in scikit-learn is very slow. The AUC scores (for the whole training set) yielded by SVM on the PCA transformed dataset (with the largest 100 eigenvalues) barely get above 0.50. For this reason, we think that employing SVM on this dataset is not fruitful and did not go any further.

\subsection{K-nearest-neighbors Classifier}

Set the number of neighbors to 10. Before PCA, the 6-fold cross-validation yields mean AUC scores of 0.68 for upselling, 0.583 for churn and 0.570 for appetency. After PCA, 0.585 for upselling, 0.550 for churn and 0.558 for appetency. PCA consistently makes the results worse.

\subsection{Decision Tree Classifier}

Decision tree classifier performs quite well compared to other classifiers. 6-fold stratified cross-validation for decision tree classifier applying on the original (before PCA-transform) dataset yields mean AUC scores of 0.678 for label upselling, 0.53 for label churn and 0.517 for label appetency. After PCA, upselling: 0.585, churn: 0.520 and appetency: 0.517. Decision tree does not seem to benefit from PCA, but PCA does not seem to affect the performance of appetency prediction. 

However, by trying out different sets of parameters, we find that we can boost the performance of decision tree by imposing a maximum value on the depth of the tree. For example, if we set the maximum to 10, we will have (without PCA) upselling: 0.814, churn: 0.680 and appetency: 0.723. With PCA, upselling: 0.675, churn 0.618 and appetency: 0.729. 

\section{Ensemble methods}

\subsection{Random Forests}

Random Forests ensemble classifier has the best performance among all the methods we have tried. In addition to the maximum depth, the number of estimators also affects the performance significantly. By trying out different values for these parameters, we find the parameters pairs that can produce the best performance for the three labels. See Tbl. \ref{tbl:auc_statistics}.

\begin{table}
\centering
\caption{\label{tbl:auc_statistics}Means and standard deviations of AUC scores for the three labels using Random Forests. 30 independent samples are generated for each of the 3 labels.}
    \begin{tabular}{| c | c | c | c | c | c |}
    \hline
    label & mean AUC & AUC standard deviation & max depth & $\#$ of estimators & PCA used? \\ \hline
    upselling & 0.84 & 0.011 & 8 & 35 & No\\
    churn & 0.684 & 0.0057 & 8 & 35 & No\\
    appetency & 0.77 & 0.016 & 8 & 35 & Yes\\ \hline
    
    \end{tabular}
\end{table}

\subsection{Simple Combination}

Since the naïve Bayes classifier combined with PCA produce reasonable results, we expect that combining it with the random forests might give a better result. However, this turned out not to be the case. Following is a description of what we tried.

There are several ways of combining different classifiers, for example voting, averaging the classifier probabilities or combining the probabilities using a multiplication rule. Because we only had two classifiers and two nominal classes, voting wouldn't make sense. Thus we tried averaging and multiplying the probabilities given by \emph{predict\_proba} method of our scikit-learn classifiers.

The arithmetic average is computed simply by $p(\textrm{class}) = \frac{1}{2}[p_{RF}(\textrm{class}) + p_{nB}(\textrm{class})]$, where $p_{c}(\textrm{class})$ is the probability for a sample belonging to certain $\textrm{class}$ given classifier $c$. The multiplying rule is
\[
	p(\textrm{class}) = \frac{p_{RF}(\textrm{class}) \cdot p_{nB}(\textrm{class})}{p_{\textrm{prior}}(\textrm{class})},
\]
where $p_{\textrm{prior}}(\textrm{class})$ is the proportion of given class in the training data set.

Neither of these worked too well, and for classifiers constructed in both ways the AUC scores for all classes varied between 0.55 and 0.65. Having investigated closer, we noticed that naïve Bayes classifier only gives probabilities of 1 and 0.

The results were especially disappointing, especially when considering that the correlation coefficient of naïve Bayes classifier with PCA and Random Forests computed for the whole training data set is only 0.074. A low correlation coefficient would promise good results for an ensemble classifier. However, this was not a case. One explanation for a low correlation and dismal results might be that naïve Bayes gives so much worse results that it both drags the overall results down and affects the correlation coefficient.

\section{Software}
We use Python's scikit-learn\footnote{http://scikit-learn.org} (including numpy, scipy and matplotlib) and pandas\footnote{http://pandas.pydata.org} (data mangling).

Our code is available at a Git repository at: https://bitbucket.org/aaltoan/comp4332

\end{document}
