% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% for highlighting
\usepackage{color}
\usepackage{soul}

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

\title{COMP4332 Project 2 Complete Report}
\author{AALTO Antti and WU Degang}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle


\section{Introduction}
From a set of heterogeneous data, we are going to predict who a user will follow after a time period\footnote{http://www.kddcup2012.org/}.

\section{Factorization Machine Algorithm}
We will use an algorithm similar to the factorization machines algorithm used by Steffen Rendle\footnote{https://kaggle2.blob.core.windows.net/competitions/kddcup2012/2748/media/Rendle.pdf}. The learning part is handled by stochastic gradient descent. We used the collaborative filtering toolkits from \texttt{graphchi}\footnote{http://graphlab.org/projects/graphchi.html}. The selected algorithm handles all the attributes as categorical values and makes it really straightforward for an analyst to add more features to a model.

\section{Preprocessing steps}

\subsection{Remarks on the data}

In the file \texttt{rec\_log\_train.txt}, there are 73209277 records. Only 5253828 (7\%) of them are positive instances (meaning the user accept the recommendations given by the system). The dataset is, again, highly imbalanced. There are 1392873 distinct users and 4710 distinct items. Other statistics are difficult to calculate because of the sheer size of the dataset. Many of the users that are recorded in the test data do not appear in the training data. There are a huge number of duplicate ratings, i.e., instances in \texttt{rec\_log\_train.txt} among which the user id, item id and \texttt{result} fields are the same (the timestamp is the only difference). Also, there are a small number of conflicting instances. 

\subsection{Preprocessing procedures}

First note that the \texttt{result} field of the data is asymmetric: 1 means that the user likes the recommendation; -1, on the other hand, does not suggest anything on its own. Nevertheless, we still treat the result as the rating of the item. 

The steps:

\begin{enumerate}
	\item We scan through \texttt{user\_profile.txt} and \texttt{item.txt}. Instances in \texttt{user\_profile.txt} is like:
	\begin{center}
	100044	18991	5	831;55;198
	\end{center}
	To adapt it to the sparse data input format, we transform the above line to
	\begin{center}
	100044	y1899	g1	n5	i831	i55	i198
	\end{center}
	where \textit{y} stands for birth-year, \textit{g} for gender, \textit{n} for number-of-tweets and \textit{i} for user interest.
	Similarly, an instance in \texttt{item.txt} such as
	\begin{center}
	1775040	1.4.2.2	100947;97714
	\end{center}
	will be transformed to
	\begin{center}
	1775040	0c1	1c4	2c2	3c2	k100947	k97714
	\end{center}
	where \textit{c} stands for item category, \textit{k} for keywords.
	
	\item We incorporated the user action records from \texttt{user\_sns.txt} to the user profiles. The most important action done by user is that of following an item. We added these followed item ids for each user as a new categorical variable prefixed by an \emph{f}.
	One possible enhancement that we missed was that we don't check, if the user is already following the item offered for recommendation.
	
	\item Rendle noted that the timestamps can be used to enhance the recommendation results by observing the patterns in the training file. The training recommendations always come in batches, normally in groups of three recommendations for each user with the same timestamp. Rendle has derived ten different predictive features from these patterns, and he describes intuition behind them more in depth in his paper. Of those, we selected six, supposedly the strongest ones.
	\begin{enumerate}
		\item duration of the previous batch
		\item duration of the current batch
		\item duration of the next batch
		\item duration of the next next batch
		\item duration of the next next next batch
		\item incrementing batch ID
	\end{enumerate}
	Here each batch means the recommendations which were issued with the same timestamp. Batch IDs were issued for each user. The \textit{duration} of the current batch is the time difference between the next batch and the current batch. After computing the durations we categorized them to six categories split at 1, 4, 10, 30 and 60 seconds, because the algorithms of GraphLabs we used were not able to use numerical features. The features were included in \texttt{rec\_log\_train.txt} and \texttt{rec\_log\_test.txt} as their own columns.
	
	A line in \texttt{rec\_log\_train.txt} like
	\begin{center}
	1393551	1606902	-1	1319852194
	\end{center}
	will be transformed to
	\begin{center}
	1393551	1606902	-1	1319852194	dp61	d61	dn3	b2
	\end{center}
	where \textit{dp} stands for the duration of previous batch, \textit{d} for the duration of the current batch, \textit{dn} for duration of the next batch and \textit{b} for the time-ordered index of the current batch.
	\begin{center}
	dp61    d3      dn30    dnn3    dnnn3   b3
	\end{center}
\end{enumerate}

\section{Experimental Results}

Since the problem is very large, instead of making use of all ratings, we will just randomly sample 1000000 ratings for the learning process. Also, because of the limit of time, we will only run for 100 iterations for each setting.

As a baseline, when we only made use of user id, item id, and the ratings, we got 0.186305349616 as MAP$@$3 score. If we included the timestamp as one of the features, we got 0.21836392021. If we included \texttt{user\_profile.txt}, we got 0.294864172353. This shows that the user's various attributes are highly related to whether he will accept the recommendation. If we included both \texttt{user\_profile.txt} and \texttt{item.txt}, we actually got a lower score: 0.246694416351. This could suggest that the information of the items is irrelevant or even misleading. With batch duration and \texttt{user\_profile.txt}, the MAP$@$3 score is 0.263539162067. With \texttt{user\_profile.txt}, durations of previous batch, current batch, next batch and the time-ordered index, we got 0.265838308867. 

Even though GraphLab seemed really promising at first and it has already been used to achieve very good results on KDD Cup 2012 track 1 problem\footnote{http://bickson.blogspot.co.il/2012/12/collaborative-filtering-3rd-generation.html}, there some severe limitations with it when adding more features to the data set. The \texttt{gensgd} matrix factorization machine with stochastic gradient descent learning is still quite experimental, and we encountered many bugs on our way. One particularly bad bug was related to leaking of file handles, and after trying to fix it ourselves, we ended up just reporting it to the developers of GraphChi.

After these challenges, the best MAP$@$3 score we got is 0.332322810185, with \texttt{user\_profile.txt} and timestamp. The learning was performed for all ratings. The prediction results  can be accessed at https://dl.dropboxusercontent.com/u/7383429/sub\_graphlab\_no\_limit\_with\_user\_profile.csv.

Our code is available at a Git repository at: https://bitbucket.org/aaltoan/comp4332

\end{document}
