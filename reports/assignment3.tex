% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned 
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\usepackage{amsmath}
\usepackage{amsfonts}

%%% END Article customizations

\title{A survey on solutions of KDDCUP 2012 Track 1}
\author{AALTO Antti and WU Degang}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle

\section{Winning solution}
The winning solution of KDD Cup 2012\footnote{https://kaggle2.blob.core.windows.net/competitions/kddcup2012/2748/media/SJTU.pdf} solves the social recommendation problem by building a model, which 1) comprehensively utilizes the heterogeneous data, 2) addresses the cold-start problem, and which is 3) time-aware to capture popularity evolution.

The authors build a model combining a feature-based matrix factorization model and tree-based additive forest model:
\[
	\hat{r}_{ui} = \left( \sum\limits_{c \in C(u)} \alpha_c^{(u)} \mathbf{p}_c \right)^T 
		\left( \sum\limits_{c \in C(i)} \beta_c^{(i)} \mathbf{q}_c \right)
		+ \sum\limits_{c \in C(u,i)} \gamma_c^{(u,i)}
		+ \sum\limits_{s=1}^S f_{s, root(i,s)}(x_{ui}).
\]

A matrix factorization model predicting the class for user $u$ and the item $i$ $\hat{r}_{ui}$ is of the following form:
\[
	\hat{r}_{ui} = \mathbf{p}_u^T\mathbf{q}_i.
\]
Here $\mathbf{p}_u$ and $\mathbf{q}_i$ are low-dimensional vectors denoting latent factors. The authors use the properties of social networks extensively to classify users in different classes. This results in a class aware factorization model with user latent factors
\[
	\mathbf{p}_u' = \sum\limits_{c \in C(u)} \alpha_c^{(u)} \mathbf{p}_c.
\]
$\alpha_c^{(u)}$ is user $u$'s feature weight coefficient to class $c$ and $p_c$ is the latent factor learned from the data. For example, class can be an age group, gender or the fact that the user follows Kaifu Lee (and is thus his fan). An analogous weighting is done for the item factors. The full model incorporates coefficients for users ($\alpha$), items ($\beta$) and dyadic features ($\gamma$). This model is closely related to well-known SVD++. Dyadic bias (the term with $\gamma$) is not discussed in depth in the paper, so it's a bit unclear where it comes from.

The authors use the matrix factorization model to solve the cold-start problem. This means that the features selected for the model must all be those from user profiles and categorical hierarchies. With the continuous loss functions presented below, the linear systems can be solved efficiently. Specifically, the authors use stochastic gradient descent algorithm when training the model.

The paper discusses the loss functions followingly. The training set has a binary result for each record; either the user accepts or rejects the recommendation. The paper presents a the following loss function for classification:
\[
	L_{ui}^{0-1} = \delta(r_{ui}\hat{r}_{ui}), \quad \delta(x) =
	\begin{cases}
		0 & x \geq 0 \\
		1 & x < 0
	\end{cases}.
\]
Because $\delta$ is not differentiable, the authors replace it by a convex surrogate loss function denoted by $\mathcal{C}$. They two examples: $\mathcal{C}(x) = \max(1-x, 0)$ and $\mathcal{C}(x) = ln(1 + e^{-x})$. Note that their first example is not differentiable at 0, but it doesn't seem to be critical. The surrogate loss functions are used to speed up the training, and the stochastic gradient descent method is not affected by single nondifferentiable points. For computing the top-3 items for each user the authors present a pairwise loss function \[
	L_u^0  = \sum\limits_{(i,j):r_{ui}>r_{uj}} \mathcal{C}(\hat{r}_{ui}-\hat{r}_{uj}).
\]
This is again smoothened with a surrogate loss function and weighted with the difference in the ranking measure of two ranking lists given the current predictions.The paper is a little unclear about which surrogate loss function is used in different applications of the models.

The other part of the model comes from an additive forest. Tree models are a very general and widely applicable class of models, which require few parameters (primarily only tree depth and number of supporting leaves). Trees can be combined to create forests, which can be easily controlled to prevent over-fitting by limiting the tree depth. Additive forest model is gives its own classification for the user-item pair
\[
	\hat{r}_{ui} = \sum\limits_{s=1}^S f_{s,i}(x_{ui}),
\]
which can be combined with the previous predictors.

The paper presents a specific model to address the classification problem given in KDD Cup 2012. For instance, the authors find latent factors in social network, keywords and tags, taxonomy information and user sequential patterns to construct the matrix factorization model. Age, gender temporal dynamics and user activity are used to build the additive forest. The final model is the trained with the given training data set.

The authors run a 6-fold cross-validation on the training data to get a sense on how well the model performs. Results are presented for incrementally combining the identified classes and patterns and MAP@3 is observed to increase with them. The authors discuss also experiments with ensemble methods, but conclude that no improvements were achieved.

\section{Solution from Steffen Rendle}

In the solution, Rendle used a very general model called Factorization Machines (FM) \footnote{http://dl.acm.org/citation.cfm?id=2168771}. Suppose $\textbf{x}\in\mathbb{R}^p$ is the input vector, the prediction of the target, e.g., the probability that user $u$ will follow item $i$, is
\begin{equation}
\hat{y}(\textbf{x}):=w_0+\sum_{j=1}^pw_jx_j+\sum_{j=1}^p\sum_{j'=j+1}^px_jx_{j'}\sum_{f=1}^kv_{j,f}v_{j',f},
\end{equation}
where $k$ is the dimensionality of the factorization and the model parameters $\Theta=\{w_0,w_1,\cdots,w_p,v_{1,1},\cdots,v_{p,k}\}$ are
\begin{equation}
w_0\in\mathbb{R},\quad\textbf{w}\in\mathbb{R}^p,\quad V\in\mathbb{R}^{p\times k}.
\end{equation}

This mathematical form resembles a general regression problem, and hence FM itself is problem-independent. The information of the problem lies in the design of the input vector $\bold{x}$. In this survey, each component of the feature vector is called a predictor variable.

The particular model used by Rendle is 2nd order FM, which takes into account the contributions from single components $x_i$ and the pairwise interaction between $x_i$ and $x_j$. The strengths of the contributions or interactions are learnt from the training data. The interaction strength can be written as $W_{ij}\equiv W_{ji}$, and hence the matrix $W$ is symmetric. Since the diagonal terms in $W$ do not enter into FM, they can be set arbitrarily large such that $W$ becomes a positive semi-definite matrix. By Cholesky decomposition, $W$ can be decomposed into $V\,V^t$, so that $W_{ij}=\sum_f^p{V_{if}V_{f_j}}$. Then, an assumption is made that effect of pairwise interactions has a low rank, i.e., $W_{ij}\approx\sum_f^k{V_{if}V_{f_j}}$, where $k\ll p$. Now we have obtained the expression for 2nd order FM.

Given a loss function $l(y_1,y_2)$ and observed data $S$, the task for optimization can expressed as
\begin{equation}
\textrm{OPT}(S):=\underset{\Theta}{\operatorname{argmin}}\left(\sum_{(c,\bold{x},y)\in S}c\,l(\hat{y}(x|
\Theta),y)+\sum_{\theta\in\Theta}\lambda_{\theta}\theta^2\right),
\end{equation}
where $c$ is the learning weight for individual observations and $\lambda_{\theta}$ is the regularization parameters.

Choosing $\{\lambda_{\theta}\}$ could be a very tedious task. For this reason, Rendle chose Bayesian inference with Markov Chain Monte Carlo method to learn $\Theta$ and in this method, $\{\lambda_{\theta}\}$ can be treated as an additional set of parameters and hence the optimal $\{\lambda_{\theta}\}$ will be automatically calculated.

The rest is in the domain of features engineering. Rendle treated all variables as categorical variables, and then converted them to binary dummy variable. For this reason, the length of an feature vector could be extremely long, since one categorical variable could generate many predictor variables. There is little point in calculate the interactions among predictor variables belong to the same underlying categorical variable, and for this reason, those interactions were ignored by default. 

Question remains whether it is justified to remove interactions between user variables, e.g., interaction between a user's ID and a user's age. Rendle answered this question by comparing the performances, measured in MAP@3, with or without the interactions and found that the removal of interactions between user variables does not affect the performances significantly. To build an ensemble, Rendle denoted model that take interactions between user variables into account \textit{FM with user interactions} and one that does not \textit{FM without user interactions}. The linear weights ensemble shows a slight improvement over the best standalone model.

The task now is to choose relevant predictor variables. Rendle chose user ID, item ID, age of the user, gender of the user, interaction of age and gender, number of tweets, tags, keywords, set of all users that the user follows to be the primary predictor variables. Notably, Rendle did not use time directly and he dropped the \textit{user action} information. Rendle created several new features based on the time information provided. Recommendations that are made to the same user at the same time are grouped into a \textit{session}. The duration of a session is the time difference between the current session and the next session of the same user. From these two concepts, Rendle created the following features: duration of this session, duration of previous session, duration of next session, duration of next next session, duration of next next next session, session index $j$, session index in descending order, number of sessions in the next 60 seconds, number of sessions in the previous 60 seconds and visit index $l$. A \textit{visit} is a series of consecutive sessions with very small time gap.

The most notable advantage of Rendle's solution is that the learning model, FM is very general and the learning process is well mathematically-grounded. Existing methods that deal with large sparse matrix and MCMC learning can be readily applied. The price of being a general is that the success of the model lies in the feature engineering. It is not at all clear how to incorporate the information from social networks interaction into FM. In fact, Rendle barely utilized the social networks information. Moreover, Rendle did not consider at all the possibility that users' interests change with time. In conclusion, Rendle's solution serve as a clear and mathematically-grounded baseline model and a benchmark for more sophisticated solutions.

An obvious possible improvement is to consider the possibility that users' interests change with time. We can give higher learning weights to observations that occurred recently. Another possible improvement is to incorporate the \textit{user action} information into the model.

\section{Discussion}
Rendle proposes a well grounded general model, but doesn't take into consideration the informational properties of social networks. There is also no time information accounted. The winning system of KDD Cup 2012 uses exhaustively all the data and seeks to find different model-dependent relations and patterns from the social network.

One remarkable thing is that all the three best solutions make use of some latent factor model. We also took a look at the third paper\footnote{https://kaggle2.blob.core.windows.net/competitions/kddcup2012/2748/media/FICO.pdf}. The classifier constructed with FICO Model Builder, and we find it interesting that there are also off-the-shelf products that can be used to generate well-performing prediction systems. The third system includes a plethora of different models. However, the justifications for them are quite vague.

\end{document}
