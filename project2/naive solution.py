import pandas as pd
from pandas import DataFrame, Series
import graphlab as gl
import numpy as np


def output_to_file(results):
    del results['score']
    del results['rank']
    results.save('result.csv',format='csv')
    df = pd.read_csv('result.csv',dtype={'user_id':np.int32,'item_id':np.object})
    grouped = df.groupby(df['user_id'])['item_id'].apply(lambda x:' '.join(x))
    output_df = DataFrame(grouped,columns=["clicks"])
    output_df.to_csv('sub.csv',quoting=3)

if __name__ == "__main__":
    data = gl.SFrame.read_csv('track1/rec_log_train.txt', header=False, delimiter='\t', column_type_hints=[str,str,int,str])
    data.rename({'X1': 'user_id', 'X2': 'item_id', 'X3': 'accept', 'X4': 'time_stamp'})
    filtered_data = data[data['accept'] == 1]

    # model = gl.recommender.create(data, user="X1", item="X2", target="X3", D=20)

    (train_set, test_set) = filtered_data.random_split(0.8, seed=1)

    popularity_model = gl.recommender.popularity.create(train_set, 'user_id', 'item_id')

    item_sim_model = gl.recommender.item_similarity.create(train_set, 'user_id', 'item_id')

    # result = gl.recommender.compare_models(test_set.head(500), [popularity_model, item_sim_model],
    #                                        skip_set=train_set)

    query_data = gl.SFrame.read_csv('track1/rec_log_test.txt', header=False, delimiter='\t', column_type_hints=[str,str,int,str])

    query_data.rename({'X1':'user_id','X2':'item_id','X3':'accept','X4':'time_stamp'})

    # results = model.recommend(query_data, k=3)

    # popularity_results = popularity_model.recommend(query_data, k=3)

    item_sim_results = item_sim_model.recommend(query_data, k=3)

    output_to_file(item_sim_results)