from collections import deque

with open("track1/rec_log_train.txt") as fin, open("track1/rec_log_train1.txt", "w") as fout:
    deque_dict = {}
    for line in fin:
        line = line.strip()
        record = line.split("\t")
        record[3] = int(record[3])
        user_id = record[0]
        if user_id not in deque_dict:
            deque_dict[user_id] = deque()
            deque_dict[user_id].appendleft(record[1:])
        elif record[3] == deque_dict[user_id][-1][2]:
            deque_dict[user_id].appendleft(record[1:])
        else:
            next_time = record[3]
            cur_time = deque_dict[user_id][-1][2]
            time_diff = next_time - cur_time
            for cur_record in deque_dict[user_id]:
                new_str = user_id+'\t'+cur_record[0]+'\t'+cur_record[1]+'\t'+str(cur_time)+"\t"+"d"+str(time_diff)
                fout.write(new_str+'\n')
            deque_dict[user_id].clear()
            deque_dict[user_id].appendleft(record[1:])

    for user_id, rec_q in deque_dict.iteritems():
        # we cannot determine the duration of the last section
        # therefore, we use d0 to denote duration that cannot be determined

        for rec in rec_q:
            new_str = user_id+'\t'+rec[0]+'\t'+rec[1]+'\t'+str(rec[2])+"\t"+"d0"
            fout.write(new_str+'\n')

