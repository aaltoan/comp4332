#!/bin/python

import sys

recommendations = {}
with open('rec_log_test1.txt') as f_test, open('rec_log_test.txt1.predict') as f_predict:
    for tr, p in zip(f_test, f_predict):
        tr = tr.split("\t")
        user_id = tr[0]
        item_id = tr[1]
        if user_id not in recommendations:
            recommendations[user_id] = [(item_id, float(p.strip()))]
        # elif item_id not in [item[0] for item in recommendations[user_id]]:
        elif item_id not in recommendations[user_id]:
            recommendations[user_id].append((item_id, float(p.strip())))

for user_id in recommendations:
    recommendations[user_id].sort(key=lambda x: x[1], reverse=True)
    del recommendations[user_id][3:]

if len(sys.argv) > 1:
    out_filename = sys.argv[1]
else:
    out_filename = 'sub_graphlab.csv'

with open(out_filename, 'w') as f:
    for user, items in recommendations.items():
        f.write(user + ',' + ' '.join([str(item[0]) for item in items]) + "\n")
