export GRAPHCHI_ROOT=~/graphchi-cpp
rm -rf rec_log_train*.txt_* rec_log_train*.txt.* 
$GRAPHCHI_ROOT/toolkits/collaborative_filtering/gensgd \
    --training=rec_log_train.txt \
    --val_pos=2 \
    --rehash=1 \
    --max_iter=100 \
    --gensgd_mult_dec=0.999999 \
    --minval=-1 \
    --maxval=1 \
    --quiet=1 \
    --calc_error=1 \
    --file_columns=4 \
    --features=3 \
    --last_item=1 \
    --user_file=user_profile1.txt \
    --test=rec_log_test.txt

