# due to the limitation of gensgd

with open("track1/item.txt") as fin, open("track1/item1.txt", "w") as fout:
    for line in fin:
        record = line.strip()
        record = record.split('\t')
        record[1] = record[1].split('.')
        record[1] = ';'.join([str(i)+'c'+item for i, item in enumerate(record[1])]) # c for category
        record[2] = record[2].split(';')
        record[2] = ';'.join(['k'+item for item in record[2]]) # k for keyword

        new_str = '\t'.join(record)
        fout.write(new_str+'\n')
