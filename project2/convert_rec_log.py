def discretize(d):
    if d == 0:
        d = d
    elif d < 3:
        d = 3
    elif d < 10:
        d = 10
    elif d < 30:
        d = 30
    elif d < 60:
        d = 60
    else:
        d = 61
    return str(d)

# elements in users[user_id][batch]:
# row[0]: item_id
# row[1]: click
# row[2]: timestamp
# row[3]: duration of previous batch
# row[4]: duration of this batch
# row[5]: duration of next batch
# row[6]: duration of next next batch
# row[7]: duration of next next next batch
def convert_rec_log(fin, fout):
    users = {}
    for line_no, line in enumerate(fin):
        if line_no % 100000 == 0:
            print("Line " + str(line_no))
        line = line.strip()
        record = line.split("\t")
        user_id = record[0]
        item_id = record[1]
        click = record[2]
        timestamp = int(record[3])
        record[3] = timestamp
        if user_id not in users:
            # new user
            users[user_id] = []
            users[user_id].append([record[1:] + ["-1", "-1", "-1", "-1", "0"]])
        elif timestamp == users[user_id][-1][0][2]:
            # old user and old batch
            last_duration = users[user_id][-1][0][3]
            users[user_id][-1].append(record[1:] + [last_duration, "-1", "-1", "-1", "-1", "0"])
        else:
            # old user but new batch
            last_batch = users[user_id][-1]
            last_duration = discretize(timestamp - last_batch[0][2])
            for row in last_batch:
                row[4] = last_duration
            if len(users[user_id]) > 1:
                for row in users[user_id][-2]:
                    row[5] = last_duration
                if len(users[user_id]) > 2:
                    for row in users[user_id][-3]:
                        row[6] = last_duration
                    if len(users[user_id]) > 3:
                        for row in users[user_id][-4]:
                            row[7] = last_duration
            users[user_id].append([record[1:] + [last_duration, "-1", "-1", "-1", "-1"]])
    
    # writing out
    for user_id, user in users.iteritems():
        for batch_id, batch in enumerate(user):
            for record in batch:
                #  user_id      item_id  click  timestamp   d previous
                #  duration     d next   d nn   d nnn
                #  batch_id
                fout.write(user_id +"\t"+ record[0] +"\t"+ record[1] + "\t" + str(record[2])+ "\tdp" + record[3] +
                           "\td" + record[4]+ "\tdn" + record[5] +"\tdnn"+record[6]+ "\tdnnn" + record[7] +
                           "\tb" + str(batch_id) + "\n")

with open("track1/rec_log_test.txt") as fin, open("track1/rec_log_test1.txt", "w") as fout:
    convert_rec_log(fin, fout)
