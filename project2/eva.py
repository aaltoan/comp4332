#!/usr/bin/python
import sys

def read_associations(filename):
    """
    Read a CSV file with header in KDDCup 2012 format.
    """
    with open(filename) as f:
        contents = f.readlines()
    del contents[0]
    labels = {}
    for line in contents:
        parts = line.split(",")
        labels[parts[0]] = [f.strip() for f in parts[1].split(" ")]
    return labels

def ap(correct, predicted, n=3):
    """
    Calculate average precision for single prediction.
    """
    if not correct:
        return 1.0
    correct_count = 0
    denom = 0
    for i in range(0, min(n, len(predicted))):
        if predicted[i] in correct:
            correct_count += 1
            denom += 1.0 * correct_count / (i + 1)
    return denom / min(n, len(correct))

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: eva.py <correct file> <predicted file>")
        exit()
    correct_file = sys.argv[1]
    predicted_file = sys.argv[2]

    correct = read_associations(correct_file)
    predicted = read_associations(predicted_file)

    total = 0
    for uid, p in predicted.iteritems():
        total += ap(correct[uid], p)
    print(total / len(predicted))
