# due to the limitation of gensgd

users_following = {}
with open("track1/user_sns.txt") as f:
    for line in f:
        record = line.strip()
        record = record.split('\t')
        if record[0] in users_following:
            users_following[record[0]].append('f'+record[1]) # f for following
        else:
            users_following[record[0]] = ['f'+record[1]]

with open("track1/user_profile.txt") as fin, open("track1/user_profile1.txt", "w") as fout:
    for line in fin:
        record = line.strip()
        record = record.split('\t')
        record[1] = 'y' + record[1] # y for year
        record[2] = 'g' + record[2] # g for gender
        record[3] = 'n' + record[3] # t for number-of-tweet
        record[4] = record[4].split(';')
        record[4] = ';'.join(['i'+item for item in record[4]]) # i for interest
        if record[0] in users_following:
            record.append(';'.join(users_following[record[0]]))
        new_str = '\t'.join(record)
        fout.write(new_str+'\n')